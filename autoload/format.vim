" File: format.vim
" Author: Kenneth Benzie <k.benzie83@gmail.com>
" Description: formatexpr for clang-format & yapf

if !exists('g:loaded_format') || &compatible
  finish
endif

" set debug=msg,throw  " uncomment to see error messages
pythonx import format

function! format#clang_format()
  pythonx format.clang_format()
endfunction

function! format#yapf()
  pythonx format.yapf()
endfunction
