" File: format.vim
" Author: Kenneth Benzie <k.benzie83@gmail.com>
" Description: formatexpr for clang-format & yapf

" Set Up {{{
if exists('g:loaded_format') || &compatible
  finish
endif

if !has('pythonx')
  echoerr 'format.vim reuires Python to be enabled.'
endif

let g:loaded_format = 1
" }}}

" Options {{{
let g:clang_format_path = get(g:, 'clang_format_path', 'clang-format')
let g:clang_format_style = get(g:, 'clang_format_style', 'Google')
let g:clang_format_filetypes = get(g:, 'clang_format_filetypes',
      \ ['c', 'cpp', 'java', 'javascript', 'objc', 'objcpp', 'proto'])

let g:yapf_path = get(g:, 'yapf_path', 'yapf')
let g:yapf_style = get(g:, 'yapf_style', 'pep8')
let g:yapf_filetypes = get(g:, 'yapf_filetypes', ['python'])
" }}}

" Mappings {{{
" TODO: Renamp gq to insert a marker in order to save the current cursor
" position before the motion is invoked so it can be passes onto clang-format,
" which returns an updated cursor position after formatting. Make this
" optional.
" }}}

" Auto Command Group {{{
augroup format_filetype_group
  for s:ft in g:clang_format_filetypes
    exec 'autocmd FileType '.s:ft.' setlocal formatexpr=format#clang_format()'
  endfor

  for s:ft in g:yapf_filetypes
    exec 'autocmd FileType '.s:ft.' setlocal formatexpr=format#yapf()'
  endfor
augroup end
" }}}
